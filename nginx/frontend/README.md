# Frontend

## add new tab

after defining a new dashboard in Grafana, you may want to add it to your user interface. To do so you need to modify the index.html file

* create a new tab in the nav-tab list (lines 56-61)
* create a new tab pane in the tab-content section (lines 63-78)
* add the link to your dashboard using an iframe element