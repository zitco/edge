#!/bin/sh

HOME=$PWD

docker network create bredge

cd $HOME/nginx && ./build.sh
cd $HOME/node-red && ./build.sh
cd $HOME/grafana && ./build.sh

cd $HOME/services/opcua-adapter && npm install && npm run build
cd $HOME/services/opcua-manager && npm install && npm run build
cd $HOME/services/data-ingester && npm install && npm run build
