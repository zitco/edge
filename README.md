# needs update!!! 

# Industrial IoT Demo

using OPC UA connection to PLC

## Architecture

![Demo Architecture](frontend/schema.png "Demo Architecture")

### OPC UA server

used PLC simulator: https://hub.docker.com/r/hzei/plc-simulator  
connection to other OPC UA server (like a physical PLC) can be easily done through Node Red configuration

### Node Red

defines the workflow to read data from the OPC UA server and store measures in the Influx time series database
additional nodes for OPC UA and InfluxDB provided

### InfluxDB

measurements archive

### Grafana

data visualization

the demo provides 2 default dashboards

grafana runs in anonymous kiosk mode by default. you can log in by using grafana default credentials (admin/admin) to modify settings and/or add new dashboards

### Nginx

the frontend is served by Nginx (https://hub.docker.com/_/nginx)


## Requirements

- docker
- docker-compose

## Installation

#### using git

````
git clone https://gitlab.com/zitco/briiotdemo.git
cd briiotdemo
docker-compose up
````

#### download archive

````
wget https://gitlab.com/zitco/briiotdemo/-/archive/V1.0/briiotdemo-V1.0.tar.gz
tar -xf briiotdemo-V1.0.tar.gz
cd briiotdemo-V1.0
docker-compose up
````



## First Steps

- open your browser and connect to http://localhost (or whereever you host the demo)

## Add new OPC UA data source

Use node Node Red for the data flow configuration. It's recommended to add a new flow. Then use the opcua nodes to define your flow. Documentation for the certain nodes is provided in Node Red.

## Modify dashboards

You need to be logged into Grafana (admin/admin) to be able to edit dashboards. After adding new dashbaords you can add them to the frontend as described in https://gitlab.com/zitco/iiotdemo/-/blob/master/frontend


