#!/bin/sh

HOME=$PWD

docker network create bredge

cd $HOME/.dev/typemon && npm run build

cd $HOME/services/opcua-adapter && npm install
cd $HOME/services/opcua-manager && npm install
cd $HOME/services/data-ingester && npm install
