let socket = io();

angular.module('myapp', [])
.factory('DataService', function ($http) {
    return {
        init: function() {
            socket.emit('init');
        },
        connect: function(id) {
            socket.emit('connectDevice', id);
        },
        disconnect: function(id) {
            socket.emit('disconnectDevice', id);
        },
        browse: function(id) {
            socket.emit('browse', id);
        },
		getDevices: function() {
            socket.emit('getDevices');
//            return $http.get('/devices');
        },
		saveDevice: function(data) {
			return $http.post('saveDevice', data);
		},
		saveSubscriptions: function(id, data) {
			return $http.post('subscriptions/'+id, {nodes: data});
		}
    }
})
.controller('OpcuaController', function($scope, DataService) {
	let self = this;

	$scope.headline = 'OPC UA Manager';
    $scope.checkedItems = [];
	
	this.devices = [];

	this.tree = {};

	this.initNew = () => {
		return {
			id: null,
			alias: null,
			type: 'opcua',
			config: {
				url: null,
				autoconnect: false
			}
		}
	}
	this.form = this.initNew();

    this.get = () => {
    	console.log();
    }

    this.loadConfig = () => {
    	console.log('load config');
		DataService.init();
    }
    
	this.connect = (id) => {
		DataService.connect(id);
	}
	
	this.disconnect = (id) => {
		DataService.disconnect(id);
	}
	
	this.browse = (id) => {
		DataService.browse(id);
	}

	this.saveSubscriptions = (id) => {
		console.log('save subscription for', id);

		this.devices[this.devices.findIndex((d) => d.id == id)].subscriptions = this.tree[id].values;
		self.devices[this.devices.findIndex((d) => d.id == id)].dirty = false;
		DataService.saveSubscriptions(id, this.tree[id].values);
	}

	this.setTree = (device) => {
		console.log('set tree', device);

		this.tree[device.id] = new Tree('#tree-'+device.id, {
			data: device.nodes,
			closeDepth: 3,
			loaded: function () {
				this.values = device.subscriptions || [];
				console.log(this.selectedNodes);
				console.log(this.values);
				self.devices[self.devices.findIndex((d) => d.id == device.id)].dirty = false;
			},
			onChange: function () {
				self.devices[self.devices.findIndex((d) => d.id == device.id)].dirty = true;
				console.log('changed', this.selectedNodes, this.values);
				
			}
		})
	}	
	
	this.updateDevice = (device) => {
		let i = this.devices.findIndex((d) => d.id == device.id);
		this.devices[i] = device;
		if (device.nodes) {
			this.setTree(device);
		}
		console.log(this.devices);
	}

	this.editDevice = (device) => {
		
		this.form = device;
		
		console.log(this.form);
		$("#devicePopup").modal("show");

	}

	this.saveDevice = () => {
		if (this.form && this.form.alias && this.form.config.url) {
			DataService.saveDevice(this.form);
			this.form = this.initNew();
		}
		$("#devicePopup").modal("hide");
	}

    $scope.$watch('nodes', () => {
//    	console.log($scope.nodes);
    	socket.emit('nodes', $scope.nodes);
    }, true);

    socket.on('updateDevice', (msg) => {
		$scope.$apply(() => {
			this.updateDevice(msg);
		});
	});

    socket.on('message', (msg) => {
    	$scope.$apply(() => {
	    	$scope.message = msg;
    		console.log($scope.message);
    	});
    });

	socket.on('devices', (msg) => {
		console.log('message received', msg);
		$scope.$apply(() => {
			self.devices = msg;
		});
		for (let i in self.devices) {
			if (self.devices[i].nodes) {
				self.setTree(self.devices[i]);
			}
		}
	});

	DataService.getDevices();

})



  