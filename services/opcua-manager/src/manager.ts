import * as fs from "fs";
import * as yaml from "js-yaml";
import * as docker from './docker';

import { MqttAdapter } from "./mqtt-adapter";

import { uniqueid } from "./tools";

const configFile = './opcua.yaml';

const applicationName = process.env.APPLICATION_NAME || 'default';
const serviceName = process.env.SERVICE_NAME || 'opcua-manager';
const mqttBroker = process.env.MQTT_BROKER || 'mqtt://localhost';
const opcuaAdapter = process.env.OPCUA_ADAPTER || 'opcua-adapter';
const deviceManager = process.env.DEVICE_MANAGER || 'device-manager';
const opcuaManager = process.env.OPCUA_MANAGER || 'opcua-manager';
const opcuaServer = process.env.OPCUA_SERVER || 'opc.tcp://localhost:4840';

const mqtt = new MqttAdapter(process.env.MQTT_BROKER);

export class Manager {

    private config: any;
    private settings: any;
    private devices: any;

    public emitter: any;

    constructor() {
        const self = this;
        let nodes = [];

        mqtt.subscribe(`${applicationName}/${serviceName}/browseResult/#`, function (topic: string, payload: any) {
            let tpx = topic.split('/');

            nodes = JSON.parse(payload.toString());

            let device = self.devices.find((d: any) => d.id == tpx[3]);
            device.nodes = nodes;

            if (self.emitter) self.emitter('updateDevice', device);

        })
        
        mqtt.subscribe(`${applicationName}/${serviceName}/status/report`, function (topic: string, payload: any) {
            let status = JSON.parse(payload.toString());
            let device = self.devices.find((d: any) => d.id == status.deviceId);
            device.status = status.status;
            if (device.status == "Online") {
                mqtt.publish(`${applicationName}/${opcuaAdapter}/${device.id}/subscribe`, device);
            }
            if (self.emitter) self.emitter('updateDevice', device);
            self.saveConfig();
        })

        this.loadConfig();

    }

    private startDevice(device: any) {

        console.log('start device', device);
        let settings = this.settings[device.type];
        device.status = "Offline";

        docker.ps(`name="${device.id}"`)
        .then((running: any) => {
            console.log(running);
    
            if (running.services.length) {
                console.log(`device ${device.id} is already running`);
//                mqtt.publish(`${applicationName}/${opcuaAdapter}/${device.id}/status/get`, {});
                mqtt.publish(`${applicationName}/${opcuaAdapter}/${device.id}/reconnect`, {});
            } else {
                console.log('docker run ' + device.id);
                let payload = {
                    image: settings.image,
                    name: device.id,
                    port: settings.port,
                    network: settings.network,
                    environment: [
                        `APPLICATION_NAME=${applicationName}`, 
                        `MQTT_BROKER=${mqttBroker}`, 
                        `SERVICE_NAME=opcua-adapter`, 
                        `DEVICE_ID=${device.id}`, 
                        `INSTANCE_NAME=${device.alias}`, 
                        `OPCUA_SERVER=${device.config.url}`,
                        `DEVICE_MANAGER=${serviceName}`,
                        `OPCUA_MANAGER=${serviceName}`,
                    ]
                };
                docker.run(payload);
            }

        })
        .catch((error) => {
            console.error (error);
        })

        return device;
    
    }

    private stopDevice(device: any) {
        device.status = "Offline";

        docker.ps(`name="${device.id}"`)
        .then((running: any) => {
            console.log(running);
    
            if (running.services.length) {

                console.log('docker run ' + device.id);
                let payload = {
                    name: device.id
                };
                docker.stop(payload);
            }
        })
        return device;
    }

    public connectDevice(id: any) {
        console.log('connect device', id);
        let device = this.startDevice(this.devices[this.devices.findIndex((d: any) => d.id == id)]);
        this.saveConfig();
        return device;
    }

    public disconnectDevice(id: any) {
        console.log('disconnect device', id);
        let device = this.stopDevice(this.devices[this.devices.findIndex((d: any) => d.id == id)]);
        this.saveConfig();
        return device;
    }

    public saveDevice(device: any) {
        console.log('save device', device);
        if (device.id) {
            this.devices[this.devices.findIndex((d: any) => d.id == device.id)] = device;
        } else {
            device.id = uniqueid();
            device.status = "Offline";
            if (device.config && device.config.autoconnect) {
                device = this.startDevice(device);
            }
            this.devices.push(device);
        }
        this.saveConfig();
        if (this.emitter) this.emitter('devices', this.devices);

    }

    public browse(id: any) {
        let device = this.devices[this.devices.findIndex((d: any) => d.id == id)];
        mqtt.publish(`${applicationName}/${opcuaAdapter}/${device.id}/browse`, {});
    }

    public getNodes()  {
        return [];    
    }

    public getDevices() {
        return this.devices;
    }

    public saveSubscriptions(id: any, nodes: any) {
        console.log('save subscriptions', id, nodes);
        let device = this.devices[this.devices.findIndex((d: any) => d.id == id)];
        device.subscriptions = nodes;
        mqtt.publish(`${applicationName}/${opcuaAdapter}/${device.id}/subscribe`, device);
        this.saveConfig();
    }

    private loadConfig(callback?: any) {
        console.log('loading configuration');
        this.config = yaml.load(fs.readFileSync(configFile, 'utf8'));
        this.settings = this.config.settings;        
        this.devices = this.config.devices;        
        
        //
        // autoconnect devices
        //
        for (let i in this.devices) {
            if (this.devices[i].config && (this.devices[i].config.autoconnect || this.devices[i].status == 'Online')) {
                this.devices[i] = this.startDevice(this.devices[i]);
            }
        }
        this.saveConfig();

        if (callback) {
            callback(this.devices);
        }
    }

    private saveConfig(callback?: any) {
        let y = yaml.dump(this.config);
        fs.writeFile(configFile, y, (err) => {
            if (err) {
                console.log(err);
            }
        });
    }

    public init(callback?: any) {
        this.loadConfig(callback);
    }


}


