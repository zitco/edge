const fs = require("fs");
const yaml = require("js-yaml");
const Mqtt = require("mqtt");

const configFile = './opcua.yaml';

const applicationName = process.env.APPLICATION_NAME || 'default';
const serviceName = process.env.SERVICE_NAME || 'opcua-manager';
const mqttBroker = process.env.MQTT_BROKER || 'mqtt://localhost';
const instanceName = process.env.INSTANCE_NAME || 'opcua-adapter';
const deviceManager = process.env.DEVICE_MANAGER || 'device-manager';
const opcuaManager = process.env.OPCUA_MANAGER || 'opcua-manager';
const opcuaServer = process.env.OPCUA_SERVER || 'opc.tcp://localhost:4840';



let config = yaml.load(fs.readFileSync(configFile, 'utf8'));
let nodes = []; //config.devices[0].metrics;

const mqtt  = Mqtt.connect(mqttBroker);

console.log(nodes);

mqtt.on('connect', function () {
    let topic = `${applicationName}/${serviceName}/#`;

    mqtt.subscribe(topic, function (err) {
        if (!err) {
          console.log('subscribed to ', topic);
        }
    })
  
    
    mqtt.publish(`${applicationName}/opcua-adapter/browse`, '');

});

mqtt.on('message', (topic, payload) => {
//    console.log('message received', topic);
    switch(topic) {
        case `${applicationName}/${serviceName}/browseResult`:
            nodes = JSON.parse(payload.toString());
//            console.log('receive tree update', nodes);
            break;
    }
})

const getNodes = () => {

    return nodes;    
}


module.exports = {
    getNodes
};



