const shell = require("shelljs");
const dockercmd = 'docker';

export const ps = async (filter?: string) => {
    return new Promise((resolve, reject) => {
      shell.exec(`${dockercmd} ps --filter "${filter}" --format \'{{json .}}\'`, { silent: true }, (error:any , stdout:any , stderr:any ) => {
        if (!error) {
          const response = stdout.split('\n')
          response.pop()
          const services = '[' + response.join(',') + ']'
          const eventPayload =
          {
            command: 'docker ps',
            services: JSON.parse(services),
            status: 'ok'
          }
          return resolve(eventPayload)
        } else {
          const eventPayload = {
            command: 'docker ps',
            response: stderr,
            status: 'error'
          }
          return reject(eventPayload)
        }
      })
    })
  }


export const run = (config: any) => {
	let cmd = `${dockercmd} run -d --rm`;
	let name = config.name || config.image;
	if (config.port) 	cmd += ' -p ' + config.port;
    if (config.network) cmd += ' --network ' + config.network;
    if (config.volumens) {
        for (let vol of config.volumes) {
            cmd += ' -v ' + vol;
        }
    }
    if (config.environment) {
        for (let env of config.environment) {
            cmd += ' --env ' + env;
        }
    }
	cmd += ' --name ' + name;
	cmd += ' ' + config.image;
	console.log(cmd);
  	shell.exec(cmd, { silent: true });
}


export const stop = (config: any) => {
	let cmd = `${dockercmd} container stop `;
	let name = config.name || config.image;
	cmd += ' ' + name;
	console.log(cmd);
  	shell.exec(cmd, { silent: true });
}

export const execute = (cmd: string, param: any) => {
	console.log('execute docker: '+cmd);
	console.log(param);
	switch(cmd) {
		case "run":
			run(param);
			break;			
		case "stop":
			stop(param);
			break;			
	}
} 