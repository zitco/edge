console.log('starting app');

import { server } from "./app";

server.listen(8080, () => {
	console.log('listening on port 8080');
})