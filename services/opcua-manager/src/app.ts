import express from "express";
import * as bodyParser from "body-parser";
import * as path from "path";
import { Manager } from "./manager";

const dummy = {
	status: 'OK'
}

export const app = express();

export const server = require("http").Server(app);

const io = require("socket.io")(server);
const manager = new Manager();
const version = '0.1';

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', './views');
app.use (bodyParser.json({limit: '10mb'}));
app.use (bodyParser.urlencoded({extended: true}));

app.use(express.static(path.join(__dirname, '..', 'views')));

app.get('/devices', async (req, res) => {
	let devices = manager.getDevices();
	console.log(devices);
	res.json(devices);
});

app.get('/nodes', async (req, res) => {
	let nodes = manager.getNodes();
	console.log(nodes);
	res.json(nodes);
});

app.get('/test', async (req, res) => {
	res.send(dummy);
})

app.get('/get', async (req, res) => {
	res.send(dummy);
})

app.get('/all', async (req, res) => {
	res.send(dummy);
})

app.get('/', async (req, res) => {
	res.render('index.html');
});

app.post('/saveDevice', async (req, res) => {
	manager.saveDevice(req.body);

	res.send(dummy);
});

app.post('/subscriptions/:id', async (req, res) => {
	manager.saveSubscriptions(req.params.id, req.body.nodes);

	res.send(dummy);
});



io.on('connection', function(socket: any) {
	console.log('client connection established');
	socket.on('init', function(msg: any) {
		socket.emit('devices', manager.getDevices());
		manager.init((devices: any) => {
			socket.emit('devices', devices);
		});
	})
	socket.on('getDevices', function() {
		socket.emit('devices', manager.getDevices());
	})
	socket.on('connectDevice', function(id: any) {
		manager.connectDevice(id);
	})
	socket.on('disconnectDevice', function(id: any) {
		manager.disconnectDevice(id);
		socket.emit('devices', manager.getDevices());
	})
	socket.on('browse', function(id: any) {
		manager.browse(id);
	})

})

manager.emitter = (topic: string, payload: any) => {
	io.emit(topic, payload);
}


