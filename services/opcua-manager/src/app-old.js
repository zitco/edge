const express = require("express");
const http = require("http");
const socketio = require('socket.io');
const bodyParser = require("body-parser");
const path = require('path');
const manager = require("./manager");

const dummy = {
	status: 'OK'
}

const app = express();
app.server = http.createServer(app);
app.io = socketio(app.server);

app.version = '0.1';

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', './views');
app.use (bodyParser.json());
app.use (bodyParser.urlencoded({extended: true}));

app.use(express.static(path.join(__dirname, '..', 'views')));

app.get('/nodes', async (req, res) => {
	let nodes = manager.getNodes();
	console.log(nodes);
	res.json(nodes);
});

app.get('/get', async (req, res) => {
	res.send(dummy);
})

app.get('/all', async (req, res) => {
	res.send(dummy);
})

app.get('/', async (req, res) => {
	res.render('index.html');
});

app.io.on('connection', function(socket) {
	console.log('client connection established');
	socket.on('nodes', function(msg) {
		console.log('Nodes:' + msg);
	})
})

app.message = function(msg) {
	console.log(msg);
	app.io.emit('message', msg);
}

module.exports = app;

