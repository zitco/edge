import { connect } from "mqtt";

export class MqttAdapter {

    private client: any;
    private brokerUrl: string = "mqtt://localhost";

    constructor(brokerUrl?: string) {
        let self = this;
    	if (brokerUrl) this.brokerUrl = brokerUrl;
        this.client = connect(this.brokerUrl);
	    this.client.on("connect", () => {
            console.log(`MQTT broker (${brokerUrl}) connected`);
        });
    }

    publish(topic: string, message: any) {
        if (typeof message !== 'string') {
            message = JSON.stringify(message);
        }
        this.client.publish(topic, message);
    };

    subscribe(subscribeToTopic: string, callback: any) {
    	this.client.subscribe(subscribeToTopic, (err: any) => {
    		if (!err) {
    			console.log(`MQTT topic ${subscribeToTopic} subscribed`);
    		}
    	});
    	this.client.on('message', (topic:string, message:any) => {

            console.log('message received', topic);

            let to = topic.split('/');
            let st = subscribeToTopic.split('/');
            let match = true;

            for (let i in to) {
                if (st[i] == '#') break;
                if (to[i] != st[i]) {
                    match = false;
                    break;
                }
            }

            if (match && callback) {
                callback(topic, message);
            }
    	});
    }
    

}



