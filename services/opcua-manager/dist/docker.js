"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.execute = exports.stop = exports.run = exports.ps = void 0;
var shell = require("shelljs");
var dockercmd = 'docker';
var ps = function (filter) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, new Promise(function (resolve, reject) {
                shell.exec("".concat(dockercmd, " ps --filter \"").concat(filter, "\" --format '{{json .}}'"), { silent: true }, function (error, stdout, stderr) {
                    if (!error) {
                        var response = stdout.split('\n');
                        response.pop();
                        var services = '[' + response.join(',') + ']';
                        var eventPayload = {
                            command: 'docker ps',
                            services: JSON.parse(services),
                            status: 'ok'
                        };
                        return resolve(eventPayload);
                    }
                    else {
                        var eventPayload = {
                            command: 'docker ps',
                            response: stderr,
                            status: 'error'
                        };
                        return reject(eventPayload);
                    }
                });
            })];
    });
}); };
exports.ps = ps;
var run = function (config) {
    var cmd = "".concat(dockercmd, " run -d --rm");
    var name = config.name || config.image;
    if (config.port)
        cmd += ' -p ' + config.port;
    if (config.network)
        cmd += ' --network ' + config.network;
    if (config.volumens) {
        for (var _i = 0, _a = config.volumes; _i < _a.length; _i++) {
            var vol = _a[_i];
            cmd += ' -v ' + vol;
        }
    }
    if (config.environment) {
        for (var _b = 0, _c = config.environment; _b < _c.length; _b++) {
            var env = _c[_b];
            cmd += ' --env ' + env;
        }
    }
    cmd += ' --name ' + name;
    cmd += ' ' + config.image;
    console.log(cmd);
    shell.exec(cmd, { silent: true });
};
exports.run = run;
var stop = function (config) {
    var cmd = "".concat(dockercmd, " container stop ");
    var name = config.name || config.image;
    cmd += ' ' + name;
    console.log(cmd);
    shell.exec(cmd, { silent: true });
};
exports.stop = stop;
var execute = function (cmd, param) {
    console.log('execute docker: ' + cmd);
    console.log(param);
    switch (cmd) {
        case "run":
            (0, exports.run)(param);
            break;
        case "stop":
            (0, exports.stop)(param);
            break;
    }
};
exports.execute = execute;
