"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.uniqueid = void 0;
function uniqueid() {
    function chr4() {
        return Math.random().toString(16).slice(-4);
    }
    return chr4() + chr4() + chr4();
}
exports.uniqueid = uniqueid;
