"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Manager = void 0;
var fs = __importStar(require("fs"));
var yaml = __importStar(require("js-yaml"));
var docker = __importStar(require("./docker"));
var mqtt_adapter_1 = require("./mqtt-adapter");
var tools_1 = require("./tools");
var configFile = './opcua.yaml';
var applicationName = process.env.APPLICATION_NAME || 'default';
var serviceName = process.env.SERVICE_NAME || 'opcua-manager';
var mqttBroker = process.env.MQTT_BROKER || 'mqtt://localhost';
var opcuaAdapter = process.env.OPCUA_ADAPTER || 'opcua-adapter';
var deviceManager = process.env.DEVICE_MANAGER || 'device-manager';
var opcuaManager = process.env.OPCUA_MANAGER || 'opcua-manager';
var opcuaServer = process.env.OPCUA_SERVER || 'opc.tcp://localhost:4840';
var mqtt = new mqtt_adapter_1.MqttAdapter(process.env.MQTT_BROKER);
var Manager = /** @class */ (function () {
    function Manager() {
        var self = this;
        var nodes = [];
        mqtt.subscribe("".concat(applicationName, "/").concat(serviceName, "/browseResult/#"), function (topic, payload) {
            var tpx = topic.split('/');
            nodes = JSON.parse(payload.toString());
            var device = self.devices.find(function (d) { return d.id == tpx[3]; });
            device.nodes = nodes;
            if (self.emitter)
                self.emitter('updateDevice', device);
        });
        mqtt.subscribe("".concat(applicationName, "/").concat(serviceName, "/status/report"), function (topic, payload) {
            var status = JSON.parse(payload.toString());
            var device = self.devices.find(function (d) { return d.id == status.deviceId; });
            device.status = status.status;
            if (device.status == "Online") {
                mqtt.publish("".concat(applicationName, "/").concat(opcuaAdapter, "/").concat(device.id, "/subscribe"), device);
            }
            if (self.emitter)
                self.emitter('updateDevice', device);
            self.saveConfig();
        });
        this.loadConfig();
    }
    Manager.prototype.startDevice = function (device) {
        console.log('start device', device);
        var settings = this.settings[device.type];
        device.status = "Offline";
        docker.ps("name=\"".concat(device.id, "\""))
            .then(function (running) {
            console.log(running);
            if (running.services.length) {
                console.log("device ".concat(device.id, " is already running"));
                //                mqtt.publish(`${applicationName}/${opcuaAdapter}/${device.id}/status/get`, {});
                mqtt.publish("".concat(applicationName, "/").concat(opcuaAdapter, "/").concat(device.id, "/reconnect"), {});
            }
            else {
                console.log('docker run ' + device.id);
                var payload = {
                    image: settings.image,
                    name: device.id,
                    port: settings.port,
                    network: settings.network,
                    environment: [
                        "APPLICATION_NAME=".concat(applicationName),
                        "MQTT_BROKER=".concat(mqttBroker),
                        "SERVICE_NAME=opcua-adapter",
                        "DEVICE_ID=".concat(device.id),
                        "INSTANCE_NAME=".concat(device.alias),
                        "OPCUA_SERVER=".concat(device.config.url),
                        "DEVICE_MANAGER=".concat(serviceName),
                        "OPCUA_MANAGER=".concat(serviceName),
                    ]
                };
                docker.run(payload);
            }
        })
            .catch(function (error) {
            console.error(error);
        });
        return device;
    };
    Manager.prototype.stopDevice = function (device) {
        device.status = "Offline";
        docker.ps("name=\"".concat(device.id, "\""))
            .then(function (running) {
            console.log(running);
            if (running.services.length) {
                console.log('docker run ' + device.id);
                var payload = {
                    name: device.id
                };
                docker.stop(payload);
            }
        });
        return device;
    };
    Manager.prototype.connectDevice = function (id) {
        console.log('connect device', id);
        var device = this.startDevice(this.devices[this.devices.findIndex(function (d) { return d.id == id; })]);
        this.saveConfig();
        return device;
    };
    Manager.prototype.disconnectDevice = function (id) {
        console.log('disconnect device', id);
        var device = this.stopDevice(this.devices[this.devices.findIndex(function (d) { return d.id == id; })]);
        this.saveConfig();
        return device;
    };
    Manager.prototype.saveDevice = function (device) {
        console.log('save device', device);
        if (device.id) {
            this.devices[this.devices.findIndex(function (d) { return d.id == device.id; })] = device;
        }
        else {
            device.id = (0, tools_1.uniqueid)();
            device.status = "Offline";
            if (device.config && device.config.autoconnect) {
                device = this.startDevice(device);
            }
            this.devices.push(device);
        }
        this.saveConfig();
        if (this.emitter)
            this.emitter('devices', this.devices);
    };
    Manager.prototype.browse = function (id) {
        var device = this.devices[this.devices.findIndex(function (d) { return d.id == id; })];
        mqtt.publish("".concat(applicationName, "/").concat(opcuaAdapter, "/").concat(device.id, "/browse"), {});
    };
    Manager.prototype.getNodes = function () {
        return [];
    };
    Manager.prototype.getDevices = function () {
        return this.devices;
    };
    Manager.prototype.saveSubscriptions = function (id, nodes) {
        console.log('save subscriptions', id, nodes);
        var device = this.devices[this.devices.findIndex(function (d) { return d.id == id; })];
        device.subscriptions = nodes;
        mqtt.publish("".concat(applicationName, "/").concat(opcuaAdapter, "/").concat(device.id, "/subscribe"), device);
        this.saveConfig();
    };
    Manager.prototype.loadConfig = function (callback) {
        console.log('loading configuration');
        this.config = yaml.load(fs.readFileSync(configFile, 'utf8'));
        this.settings = this.config.settings;
        this.devices = this.config.devices;
        //
        // autoconnect devices
        //
        for (var i in this.devices) {
            if (this.devices[i].config && (this.devices[i].config.autoconnect || this.devices[i].status == 'Online')) {
                this.devices[i] = this.startDevice(this.devices[i]);
            }
        }
        this.saveConfig();
        if (callback) {
            callback(this.devices);
        }
    };
    Manager.prototype.saveConfig = function (callback) {
        var y = yaml.dump(this.config);
        fs.writeFile(configFile, y, function (err) {
            if (err) {
                console.log(err);
            }
        });
    };
    Manager.prototype.init = function (callback) {
        this.loadConfig(callback);
    };
    return Manager;
}());
exports.Manager = Manager;
