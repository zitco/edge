"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MqttAdapter = void 0;
var mqtt_1 = require("mqtt");
var MqttAdapter = /** @class */ (function () {
    function MqttAdapter(brokerUrl) {
        this.brokerUrl = "mqtt://localhost";
        var self = this;
        if (brokerUrl)
            this.brokerUrl = brokerUrl;
        this.client = (0, mqtt_1.connect)(this.brokerUrl);
        this.client.on("connect", function () {
            console.log("MQTT broker (".concat(brokerUrl, ") connected"));
        });
    }
    MqttAdapter.prototype.publish = function (topic, message) {
        if (typeof message !== 'string') {
            message = JSON.stringify(message);
        }
        this.client.publish(topic, message);
    };
    ;
    MqttAdapter.prototype.subscribe = function (subscribeToTopic, callback) {
        this.client.subscribe(subscribeToTopic, function (err) {
            if (!err) {
                console.log("MQTT topic ".concat(subscribeToTopic, " subscribed"));
            }
        });
        this.client.on('message', function (topic, message) {
            console.log('message received', topic);
            var to = topic.split('/');
            var st = subscribeToTopic.split('/');
            var match = true;
            for (var i in to) {
                if (st[i] == '#')
                    break;
                if (to[i] != st[i]) {
                    match = false;
                    break;
                }
            }
            if (match && callback) {
                callback(topic, message);
            }
        });
    };
    return MqttAdapter;
}());
exports.MqttAdapter = MqttAdapter;
