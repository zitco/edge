"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MqttAdapter = void 0;
const mqtt_1 = require("mqtt");
class MqttAdapter {
    constructor(brokerUrl) {
        this.brokerUrl = "mqtt://localhost";
        let self = this;
        if (brokerUrl)
            this.brokerUrl = brokerUrl;
        this.client = (0, mqtt_1.connect)(this.brokerUrl);
        this.client.on("connect", () => {
            console.log(`MQTT broker (${brokerUrl}) connected`);
            self.publish('geonify/message-broker', 'connected');
        });
    }
    publish(topic, message) {
        if (typeof message !== 'string') {
            message = JSON.stringify(message);
        }
        this.client.publish(topic, message);
    }
    ;
    subscribe(subscribeToTopic, callback) {
        this.client.subscribe(subscribeToTopic, (err) => {
            if (!err) {
                console.log(`MQTT topic ${subscribeToTopic} subscribed`);
            }
        });
        this.client.on('message', (topic, message) => {
            if (topic == subscribeToTopic) {
                callback(topic, message);
            }
        });
    }
}
exports.MqttAdapter = MqttAdapter;
