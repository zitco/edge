"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Node = void 0;
const node_opcua_1 = require("node-opcua");
class Node {
    constructor(node, session, subscription) {
        this.id = node.id;
        this.node = node;
        this.session = session;
        this.subscription = subscription;
    }
    getId() {
        return this.id;
    }
    read() {
        return __awaiter(this, void 0, void 0, function* () {
            const dataValue = yield this.session.readVariableValue(this.id);
            console.log(" value = ", dataValue.toString());
        });
    }
    readValue() {
        return __awaiter(this, void 0, void 0, function* () {
            const maxAge = 0;
            const nodeToRead = {
                nodeId: this.id,
                attributeId: node_opcua_1.AttributeIds.Value,
            };
            const dataValue = yield this.session.read(nodeToRead, maxAge);
            console.log(" value ", dataValue.toString());
        });
    }
    getMonitoredItem() {
        return this.monitor;
    }
    subscribe(callback) {
        let self = this;
        const itemToMonitor = {
            nodeId: this.id,
            attributeId: node_opcua_1.AttributeIds.Value
        };
        const parameters = {
            samplingInterval: 1,
            discardOldest: true,
            queueSize: 100
        };
        this.monitor = node_opcua_1.ClientMonitoredItem.create(this.subscription, itemToMonitor, parameters, node_opcua_1.TimestampsToReturn.Both);
        this.monitor.on('changed', (dataValue) => {
            if (callback) {
                callback(dataValue);
            }
        });
        this.monitor.on("err", (message) => {
            console.error(message);
        });
    }
}
exports.Node = Node;
