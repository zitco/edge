"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpcuaAdapter = void 0;
const node_opcua_1 = require("node-opcua");
const opcua_node_1 = require("./opcua-node");
class OpcuaAdapter {
    constructor(applicationName, endpointUrl, device) {
        const opcuaOptions = {
            applicationName: applicationName,
            connectionStrategy: {
                initialDelay: 1000,
                maxRetry: 1,
            },
            securityMode: node_opcua_1.MessageSecurityMode.None,
            securityPolicy: node_opcua_1.SecurityPolicy.None,
            endpointMustExist: false,
            keepSessionAlive: true,
            reconnectOnFailure: true
        };
        this.endpointUrl = endpointUrl;
        this.device = device;
        this.client = node_opcua_1.OPCUAClient.create(opcuaOptions);
        console.log('OPC UA client created');
        this.client.on("connected", () => {
            console.log('connected');
            this.device.setOnline();
        });
        this.client.on("connection_reestablished", () => {
            this.device.setOnline();
        });
        this.client.on("close", () => {
            this.device.setOffline();
        });
        this.client.on("connection_lost", () => {
            console.log('connection lost');
            this.device.setOffline();
        });
    }
    connect(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(`connect to ${this.endpointUrl}`);
            try {
                yield this.client.connect(this.endpointUrl);
                this.session = yield this.client.createSession();
                this.session.on("session_closed", () => {
                    this.device.setOffline();
                });
                this.session.on("session_restored", () => {
                    this.device.setOnline();
                });
                this.subscription = yield node_opcua_1.ClientSubscription.create(this.session, {
                    requestedPublishingInterval: 1000,
                    requestedLifetimeCount: 100,
                    requestedMaxKeepAliveCount: 10,
                    maxNotificationsPerPublish: 1000,
                    publishingEnabled: true,
                    priority: 10
                });
                console.log(`OPC UA server (${this.endpointUrl}) connected, session created`);
                return this.session;
            }
            catch (e) {
                console.error(e);
                return false;
            }
        });
    }
    disconnect() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.session.close();
            yield this.client.disconnect();
        });
    }
    browse(node, level, parent) {
        return __awaiter(this, void 0, void 0, function* () {
            node = node || {
                id: "RootFolder",
                name: "Root",
                children: []
            };
            level = level || 0;
            //        if (level == 0) console.log(node.name);
            const browseResult = yield this.session.browse(node.id);
            //        console.log('browseResult', browseResult);
            if (browseResult.references) {
                for (let r of browseResult.references) {
                    //                    console.log(r);
                    let idtype = (r.nodeId.identifierType == 1) ? "i" : "s";
                    let id = "ns=" + r.nodeId.namespace + ";" + idtype + "=" + r.nodeId.value;
                    let path = parent + ":" + r.displayName.text;
                    //            r.id = "ns="+r.nodeId.namespace+";"+idtype+"="+r.nodeId.value;
                    let child = {
                        id: id,
                        text: r.displayName.text,
                        path: path,
                        class: r.nodeClass,
                        children: []
                    };
                    //                console.log("  ".repeat(level) + child.name);
                    switch (child.class) {
                        case 1:
                            child = yield this.browse(child, (level || 0) + 1, path);
                            break;
                        case 2:
                            //                        child = await this.browse(child, (level || 0)+1, path);
                            break;
                    }
                    node.children.push(child);
                }
                return (node);
            }
        });
    }
    ;
    findPath() {
        return __awaiter(this, void 0, void 0, function* () {
            const browsePath = (0, node_opcua_1.makeBrowsePath)("RootFolder", "/Objects/MyDevice/Availability");
            const result = yield this.session.translateBrowsePath(browsePath);
            if (result && result.targets && result.targets[0]) {
                const productNameNodeId = result.targets[0].targetId;
                console.log(" Availability nodeId = ", productNameNodeId.toString());
            }
            else {
                console.log("Node not found");
            }
        });
    }
    ;
    createNode(node) {
        if (!this.session) {
            throw new Error('no active OPC UA session');
        }
        if (!this.subscription) {
            throw new Error('no active OPC UA subscription');
        }
        return new opcua_node_1.Node(node, this.session, this.subscription);
    }
}
exports.OpcuaAdapter = OpcuaAdapter;
