"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const device_1 = require("./device");
const mqtt_adapter_1 = require("./mqtt-adapter");
const applicationName = process.env.APPLICATION_NAME || 'default';
const serviceName = process.env.SERVICE_NAME || 'opcua-adapter';
const deviceId = process.env.DEVICE_ID || '00000000';
const instanceName = process.env.INSTANCE_NAME || 'default';
const deviceManager = process.env.DEVICE_MANAGER || 'device-manager';
const opcuaManager = process.env.OPCUA_MANAGER || 'opcua-manager';
const opcuaServer = process.env.OPCUA_SERVER || 'opc.tcp://localhost:4840';
console.log("starting app");
const mqtt = new mqtt_adapter_1.MqttAdapter(process.env.MQTT_BROKER);
const reportStatus = () => {
    console.log('report status', device.getStatus());
    mqtt.publish(`${applicationName}/${opcuaManager}/status/report`, {
        "service": serviceName,
        "deviceId": deviceId,
        "instance": instanceName,
        "status": device.getStatus()
    });
};
let device = new device_1.Device(applicationName, opcuaServer, reportStatus);
const subscribe = (node) => {
    device.subscribe(node, (data) => {
        mqtt.publish(`${applicationName}/measure/read`, data);
    });
};
const subscribeSet = (metrics) => {
    device.subscribeSet(metrics, (node, data) => {
        mqtt.publish(`${applicationName}/measure/read`, [
            {
                source: {
                    type: 'opcua',
                    name: instanceName,
                    url: opcuaServer
                },
                metric: node,
                measure: data
            }
        ]);
    });
};
const unsubscribe = (metric) => {
    device.unsubscribe(metric);
};
const unsubscribeSet = (nodeset) => {
    for (let node of nodeset) {
        unsubscribe(node);
    }
};
const sendPayload = (type, payload) => {
    mqtt.publish(`${applicationName}/${opcuaManager}/${type}/${deviceId}`, payload);
};
mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/subscribe`, function (topic, message) {
    console.log(topic);
    console.log(message.toString());
    let data = JSON.parse(message);
    let subscriptions;
    if (data && data.id) {
        device.set(data);
        subscriptions = data.subscriptions || [];
    }
    else {
        subscriptions = data || [];
    }
    try {
        if (Array.isArray(subscriptions)) {
            subscribeSet(subscriptions);
        }
        else {
            subscribe(subscriptions);
        }
    }
    catch (err) {
        console.error(err);
    }
});
mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/unsubscribe`, function (topic, message) {
    let data = JSON.parse(message);
    if (Array.isArray(data)) {
        unsubscribeSet(data);
    }
    else {
        unsubscribe(data);
    }
});
mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/browse`, (topic, message) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('browse');
    let tree = yield device.browse();
    sendPayload('browseResult', tree.children);
}));
mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/test`, (topic, message) => {
    console.log(topic);
    console.log(message.toString());
});
mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/status/get`, (topic, message) => {
    console.log(topic);
    reportStatus();
});
mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/reconnect`, (topic, message) => {
    if (device.getStatus() == 'Online') {
        reportStatus();
    }
    else {
        device.connect();
    }
});
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        yield device.connect();
    });
}
main();
