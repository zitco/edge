"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Device = void 0;
const opcua_adapter_1 = require("./opcua-adapter");
const opcua_node_1 = require("./opcua-node");
class Device {
    constructor(applicationName, opcuaServer, callback) {
        this.subscriptions = Object(opcua_node_1.Node);
        this.status = "Offline";
        this.subscribe = (nodeId, callback) => {
            let node = this.findById(this.nodes, nodeId);
            let opcuaNode = {
                metric: node.text,
                id: nodeId,
                name: node.text,
                path: node.path,
                class: node.class
            };
            if (this.subscriptions[nodeId]) {
                console.log(`subscription for ${nodeId} already exists`);
            }
            else {
                console.log(`subscribe ${nodeId}`);
                let monitoredNode = this.opcua.createNode(opcuaNode);
                monitoredNode.subscribe((value) => {
                    callback(opcuaNode, value);
                });
                this.subscriptions[nodeId] = monitoredNode;
            }
        };
        this.subscribeSet = (metrics, callback) => {
            for (let n in this.subscriptions) {
                if (!metrics.includes(n)) {
                    this.unsubscribe(n);
                }
            }
            for (let nodeId of metrics) {
                if (!this.subscriptions[nodeId]) {
                    this.subscribe(nodeId, callback);
                }
            }
        };
        this.subscribe_orig = (metric, callback) => {
            //
            // only consider this metric of the node souce is OPC UA
            //
            if (metric.source && metric.source.opcua) {
                if (this.subscriptions[metric.source.opcua.node.id]) {
                    console.log(`subscription for ${metric.displayName} already exists`);
                }
                else {
                    console.log(`subscribe ${metric.displayName}`);
                    let monitoredNode = this.opcua.createNode(metric.source.opcua.node);
                    monitoredNode.subscribe((value) => {
                        callback({
                            metric: {
                                name: metric.name
                            },
                            tags: {
                                objectId: metric.objectId
                            },
                            measure: {
                                ts: value.sourceTimestamp,
                                value: value.value
                            },
                            opcua: {
                                node: metric.source.opcua.node,
                                data: value
                            }
                        });
                    });
                    this.subscriptions[metric.source.opcua.node.id] = monitoredNode;
                }
            }
        };
        this.unsubscribe = (nodeId) => {
            console.log(`unsubscribe ${nodeId}`);
            if (this.subscriptions[nodeId]) {
                if (this.subscriptions[nodeId].getMonitoredItem) {
                    this.subscriptions[nodeId].getMonitoredItem().terminate();
                }
                delete this.subscriptions[nodeId];
            }
        };
        this.browse = () => __awaiter(this, void 0, void 0, function* () {
            return yield this.opcua.browse();
        });
        this.connect = () => __awaiter(this, void 0, void 0, function* () {
            return yield this.opcua.connect();
        });
        this.getStatus = () => {
            return this.status;
        };
        this.callback = callback;
        this.opcua = new opcua_adapter_1.OpcuaAdapter(applicationName, opcuaServer, this);
    }
    set(config) {
        this.config = config;
        this.nodes = this.config.nodes;
    }
    setStatus(status) {
        console.log('set device', status);
        this.status = status;
        if (this.callback)
            this.callback();
    }
    setOnline() {
        this.setStatus("Online");
    }
    setOffline() {
        this.setStatus("Offline");
    }
    findById(tree, id) {
        for (let node of tree) {
            if (node.id == id)
                return node;
            if (node.children) {
                let child = this.findById(node.children, id);
                if (child)
                    return child;
            }
        }
        return false;
    }
}
exports.Device = Device;
