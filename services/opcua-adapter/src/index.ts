import { Device } from './device';
import { MqttAdapter } from "./mqtt-adapter";

const applicationName = process.env.APPLICATION_NAME || 'default';
const serviceName = process.env.SERVICE_NAME || 'opcua-adapter';
const deviceId = process.env.DEVICE_ID || '00000000';
const instanceName = process.env.INSTANCE_NAME || 'default';
const deviceManager = process.env.DEVICE_MANAGER || 'device-manager';
const opcuaManager = process.env.OPCUA_MANAGER || 'opcua-manager';
const opcuaServer = process.env.OPCUA_SERVER || 'opc.tcp://localhost:4840';

console.log("starting app");
const mqtt = new MqttAdapter(process.env.MQTT_BROKER);

const reportStatus = () => {
    console.log('report status', device.getStatus());
    mqtt.publish(`${applicationName}/${opcuaManager}/status/report`, { 
        "service": serviceName,
        "deviceId": deviceId,
        "instance": instanceName,
        "status": device.getStatus()
    } );
}

let device = new Device(applicationName, opcuaServer, reportStatus);

const subscribe = (node: any) => {
    device.subscribe(node, (data: any) => {
        mqtt.publish(`${applicationName}/measure/read`, data);
    })
}

const subscribeSet = (metrics: Array<any>) => {
    device.subscribeSet(metrics, (node: any, data: any) => {
        mqtt.publish(`${applicationName}/measure/read`, [
            {
                source: {
                    type: 'opcua',
                    name: instanceName,
                    url: opcuaServer
                },
                metric: node,
                measure: data
            }            
        ]);
    })
}


const unsubscribe = (metric: any) => {
    device.unsubscribe(metric);
}

const unsubscribeSet = (nodeset: Array<any>) => {
    for (let node of nodeset) {
        unsubscribe(node);
    }
}


const sendPayload = (type: string, payload: any) => {
    mqtt.publish(`${applicationName}/${opcuaManager}/${type}/${deviceId}`, payload );
}

mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/subscribe`, function(topic: string, message: any) {

    console.log(topic);
	console.log(message.toString());

    let data = JSON.parse(message);
    let subscriptions;

    if (data && data.id) {
        device.set(data);
        subscriptions = data.subscriptions || [];
    } else {
        subscriptions = data || [];
    }

    try {
        if (Array.isArray(subscriptions)) {
            subscribeSet(subscriptions);
        } else {
            subscribe(subscriptions);
        }
    } catch(err) {
        console.error(err);
    }
});

mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/unsubscribe`, function(topic: string, message: any) {
    let data = JSON.parse(message);
    if (Array.isArray(data)) {
        unsubscribeSet(data);
    } else {
        unsubscribe(data);
    }
});

mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/browse`, async (topic: string, message: any) => {
    console.log('browse');
    let tree = await device.browse();
    sendPayload('browseResult', tree.children);
});

mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/test`, (topic: string, message: any) => {
    console.log(topic);
	console.log(message.toString());
});

mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/status/get`, (topic: string, message: any) => {
    console.log(topic);
    reportStatus();
});

mqtt.subscribe(`${applicationName}/${serviceName}/${deviceId}/reconnect`, (topic: string, message: any) => {
    if (device.getStatus() == 'Online') {
        reportStatus();
    } else {
        device.connect();
    }
});

async function main() {
    await device.connect();
}

main();
