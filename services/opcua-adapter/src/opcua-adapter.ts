import {
    OPCUAClient,
    MessageSecurityMode,
    SecurityPolicy,
    AttributeIds,
    makeBrowsePath,
    ClientSession,
    ClientSubscription,
    UAString
} from "node-opcua";
import { NodeClass } from "node-opcua-types";

import { Node } from './opcua-node';


export class OpcuaAdapter {

    private session: ClientSession;
    private client: OPCUAClient;
    private subscription: ClientSubscription;
    private endpointUrl: string;
    private device: any;

    constructor(applicationName: string, endpointUrl: string, device: any) {
        const opcuaOptions = {
            applicationName: applicationName,
            connectionStrategy: {
                initialDelay: 1000,
                maxRetry: 1,
            },
            securityMode: MessageSecurityMode.None,
            securityPolicy: SecurityPolicy.None,
            endpointMustExist: false,
            keepSessionAlive: true,
            reconnectOnFailure: true
        };
        this.endpointUrl = endpointUrl;
        this.device = device;
        this.client = OPCUAClient.create(opcuaOptions);
        console.log('OPC UA client created');

        this.client.on("connected", () => {
            console.log('connected');
            this.device.setOnline();
        })
        this.client.on("connection_reestablished", () => {
            this.device.setOnline();
        })
        this.client.on("close", () => {
            this.device.setOffline();
        })
        this.client.on("connection_lost", () => {
            console.log('connection lost');
            this.device.setOffline();
        })
    }

    async connect(callback?: any) {
        console.log(`connect to ${this.endpointUrl}`);
        try {
	        await this.client.connect(this.endpointUrl);
	        this.session = await this.client.createSession();
            this.session.on("session_closed", () => {
                this.device.setOffline();
            })
            this.session.on("session_restored", () => {
                this.device.setOnline();
            })
	        this.subscription = await ClientSubscription.create(this.session, {
	            requestedPublishingInterval: 1000,
	            requestedLifetimeCount: 100,
	            requestedMaxKeepAliveCount: 10,
	            maxNotificationsPerPublish: 1000,
	            publishingEnabled: true,
	            priority: 10
	        });
            console.log(`OPC UA server (${this.endpointUrl}) connected, session created`);
	        return this.session;
	    } catch(e) {
	    	console.error(e);
	    	return false;
	    }
    }

    async disconnect() {
        await this.session.close();
        await this.client.disconnect();        
    }

    async browse(node?: any, level?: number, parent?: string) {

        node = node || {
            id: "RootFolder",
            name: "Root",
            children: []
        };

        level = level || 0;
//        if (level == 0) console.log(node.name);


        const browseResult = await this.session.browse(node.id);
        //        console.log('browseResult', browseResult);
        
        if (browseResult.references) {
    
            for (let r of browseResult.references) {
//                    console.log(r);
                let idtype = (r.nodeId.identifierType == 1) ? "i" : "s";
                let id = "ns="+r.nodeId.namespace+";"+idtype+"="+r.nodeId.value;
                let path = parent + ":" + r.displayName.text;
    //            r.id = "ns="+r.nodeId.namespace+";"+idtype+"="+r.nodeId.value;
    
                let child = {
                    id: id,
                    text: r.displayName.text,
                    path: path,
                    class: r.nodeClass,
                    children: []
                }
//                console.log("  ".repeat(level) + child.name);
                switch(child.class) {
                    case 1:
                        child = await this.browse(child, (level || 0)+1, path);
                        break;
                    case 2:
//                        child = await this.browse(child, (level || 0)+1, path);
                        break;
                }
                node.children.push(child);
            }
    
            return(node);            
        }
    };


    async findPath() {
        const browsePath = makeBrowsePath(
            "RootFolder",
            "/Objects/MyDevice/Availability"
        );
    
        const result = await this.session.translateBrowsePath(browsePath);
        if (result && result.targets && result.targets[0]) {
            const productNameNodeId = result.targets[0].targetId;
            console.log(" Availability nodeId = ", productNameNodeId.toString());
        } else {
            console.log("Node not found");
        }
    };
    
    createNode(node: Node) {
        if (!this.session) {
            throw new Error('no active OPC UA session');
        }
        if (!this.subscription) {
            throw new Error('no active OPC UA subscription');
        }
        return new Node(node, this.session, this.subscription);
    }

}

