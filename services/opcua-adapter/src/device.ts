import { OpcuaAdapter } from "./opcua-adapter";
import { Node } from './opcua-node';


export class Device {

    private config: any;
    private nodes: any;
    private subscriptions = Object(Node);
    private opcua: OpcuaAdapter;
    private status: string = "Offline";
    public callback: any;

    constructor(applicationName: string, opcuaServer: string, callback: any) {
        this.callback = callback;
        this.opcua = new OpcuaAdapter(applicationName, opcuaServer, this);
    }

    public set(config: any) {
        this.config = config;
        this.nodes = this.config.nodes;
    }

    private setStatus(status: string) {
        console.log('set device', status);
        this.status = status;
        if (this.callback) this.callback();
    }

    public setOnline() {
        this.setStatus("Online");
    }

    public setOffline() {
        this.setStatus("Offline");
    }

    private findById (tree: any, id: any) {

        for (let node of tree) {
            if (node.id == id) return node;

            if (node.children) {
                let child: any = this.findById(node.children, id);
                if (child) return child;
            }
        }
        return false;

    }

    public subscribe = (nodeId: string, callback: any) => {

        let node = this.findById(this.nodes, nodeId);

        let opcuaNode: any = {
            metric: node.text,
            id: nodeId,
            name: node.text,
            path: node.path,
            class: node.class
        };

        if (this.subscriptions[nodeId]) {
            console.log(`subscription for ${nodeId} already exists`);
        } else {
            console.log(`subscribe ${nodeId}`);
            let monitoredNode = this.opcua.createNode(opcuaNode);
            monitoredNode.subscribe((value: any) => {
                callback(opcuaNode, value);
            })
            this.subscriptions[nodeId] = monitoredNode;
        }
    }

    public subscribeSet = (metrics: Array<any>, callback: any) => {
        for (let n in this.subscriptions) {
            if (!metrics.includes(n)) {
                this.unsubscribe(n);
            }
        }
        
        for (let nodeId of metrics) {
            if (!this.subscriptions[nodeId]) {
                this.subscribe(nodeId, callback);
            }
        }
    }

    public subscribe_orig = (metric: any, callback: any) => {
        //
        // only consider this metric of the node souce is OPC UA
        //
        if (metric.source && metric.source.opcua) {
            if (this.subscriptions[metric.source.opcua.node.id]) {
                console.log(`subscription for ${metric.displayName} already exists`);
            } else {
                console.log(`subscribe ${metric.displayName}`);
                let monitoredNode = this.opcua.createNode(metric.source.opcua.node);
                monitoredNode.subscribe((value: any) => {
                    callback({
                        metric: {
                            name: metric.name
                        },
                        tags: { 
                            objectId: metric.objectId 
                        },
                        measure: {
                            ts: value.sourceTimestamp,
                            value: value.value
                        },
                        opcua: {
                            node: metric.source.opcua.node,
                            data: value
                        }
                    });
                })
                this.subscriptions[metric.source.opcua.node.id] = monitoredNode;
            }
        }
    }

    public unsubscribe = (nodeId: string) => {
        console.log(`unsubscribe ${nodeId}`);
        if (this.subscriptions[nodeId]) {
            if (this.subscriptions[nodeId].getMonitoredItem) {
                this.subscriptions[nodeId].getMonitoredItem().terminate();
            }
            delete this.subscriptions[nodeId];
        }
    }

    public browse = async () => {
        return await this.opcua.browse();
    }

    public connect = async () => {
        return await this.opcua.connect();
      }

    public getStatus = () => {
        return this.status;
    }

}

