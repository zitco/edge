import {
    ClientSession,
    AttributeIds,
    ClientSubscription,
    TimestampsToReturn,
    ClientMonitoredItem,
    DataValue
} from "node-opcua";

export interface Node {
    metric: string,
    id: string,
    name: string,
    path: string,
    class: number
}

export class Node {
    id: string;
    private session: ClientSession;
    private subscription: ClientSubscription;
    private monitor: ClientMonitoredItem;
    private node: Node;

    constructor(node: Node, session: ClientSession, subscription: ClientSubscription) {
        this.id = node.id;
        this.node = node;
        this.session = session;
        this.subscription = subscription;
    }

    getId(): string {
        return this.id;
    }

    async read() {
        const dataValue = await this.session.readVariableValue(this.id);
        console.log(" value = ", dataValue.toString());
    }

    async readValue() {
        const maxAge = 0;
        const nodeToRead = {
            nodeId: this.id,
            attributeId: AttributeIds.Value,
        };
        const dataValue = await this.session.read(nodeToRead, maxAge);
        console.log(" value ", dataValue.toString());
    }

    getMonitoredItem(): ClientMonitoredItem {
        return this.monitor;
    }

    subscribe(callback?: (dataValue: DataValue) => void) {
        let self = this;
        const itemToMonitor = {
            nodeId: this.id,
            attributeId: AttributeIds.Value
        };
        const parameters = {
            samplingInterval: 1,
            discardOldest: true,
            queueSize: 100
        };        
        this.monitor = ClientMonitoredItem.create(this.subscription, itemToMonitor, parameters, TimestampsToReturn.Both);
        this.monitor.on('changed', (dataValue: DataValue) => {
            if (callback) {
                callback(dataValue);
            }
        });        
        this.monitor.on("err", (message: string) => {
            console.error(message);
        });        
    }
}
