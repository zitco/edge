import { MqttAdapter } from "./mqtt-adapter";
import { DB } from "./influx";

const mqttBroker = process.env.MQTT_BROKER || 'mqtt://localhost';
const applicationName = process.env.APPLICATION_NAME || 'default';
const serviceName = process.env.SERVICE_NAME || 'default';
const instanceName = process.env.INSTANCE_NAME || 'default';
const databaseHost = process.env.DATABASE_HOST || 'localhost';

console.log("starting app");
const mqtt = new MqttAdapter(mqttBroker, applicationName);
const db = new DB(databaseHost, applicationName);

mqtt.subscribe(`${applicationName}/measure/write/influx`, function(topic: string, message: any) {
    let data = JSON.parse(message);
    db.insert(data);
});
