import { connect } from "mqtt";

export class MqttAdapter {

    private client: any;
    private brokerUrl: string = "mqtt://localhost";

    constructor(brokerUrl?: string, applicationName?: string) {
        let self = this;
    	if (brokerUrl) this.brokerUrl = brokerUrl;
        this.client = connect(this.brokerUrl);
	    this.client.on("connect", () => {
            console.log(`MQTT broker (${brokerUrl}) connected`);
            self.publish(`${applicationName}/message-broker`, 'connected');
        });
    }

    publish(topic: string, message: any) {
        if (typeof message !== 'string') {
            message = JSON.stringify(message);
        }
        this.client.publish(topic, message);
    };

    subscribe(subscribeToTopic: string, callback: any) {
    	this.client.subscribe(subscribeToTopic, (err: any) => {
    		if (!err) {
    			console.log(`MQTT topic ${subscribeToTopic} subscribed`);
    		}
    	});
    	this.client.on('message', (topic:string, message:any) => {
            if (topic == subscribeToTopic) {
                callback(topic, message);
            }
    	});
    }
    

}



