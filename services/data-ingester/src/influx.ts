import { InfluxDB } from "influx";
export class DB {
    private influx: any;
    private databaseName: string;

    constructor(databaseHost: string, databaseName: string) {
        this.databaseName = databaseName;
        this.influx = new InfluxDB({
            host: databaseHost,
            database: databaseName
        });
        this.init();
    }

    init() {
        let self = this;
        this.influx
            .getDatabaseNames()
            .then((names: any) => {
                if (!names.includes(this.databaseName)) {
                    console.log(`create database ${this.databaseName}`);
                    return self.influx.createDatabase(this.databaseName);
                }
            })
            .catch((err: any) => {
                console.error(`Error creating Influx database!`);
            });
    }

    getTimeStamp(data: any) {
        let ts: any;
        if (data.source.type == 'opcua' && data.measure.sourceTimestamp) {
            if (data.measure.sourcePicoseconds) {
                ts = Date.parse(data.measure.sourceTimestamp).toString() + data.measure.sourcePicoseconds.toString();
                ts = ts.substr(0, 19);
            } else {
                ts = Date.parse(data.measure.sourceTimestamp).toString();
                ts = ts.substr(0, 19);
            }
        }
        if (data.timestamp) {
            ts = data.timestamp * 1000000;
        }
        return ts;
    }

    insert(data: any) {
//        console.log(data);

        if (typeof data !== "object") {
            console.error('payload is not an object');
            return false;
        }

        try {
            let ts = this.getTimeStamp(data);
            this.influx.writePoints([
                {
                    measurement: data.metric.name,
                    tags: data.metric.tags,
                    fields: { value: data.measure.value.value },
                    timestamp: ts
                }
            ]).catch((err: any) => {
                console.error(`Error saving data to InfluxDB! ${err.stack}`)
            })        
        } catch(error) {
            console.error(error);
        }
    }
}
