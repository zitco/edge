"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DB = void 0;
var influx_1 = require("influx");
var DB = /** @class */ (function () {
    function DB(databaseHost, databaseName) {
        this.databaseName = databaseName;
        this.influx = new influx_1.InfluxDB({
            host: databaseHost,
            database: databaseName
        });
        this.init();
    }
    DB.prototype.init = function () {
        var _this = this;
        var self = this;
        this.influx
            .getDatabaseNames()
            .then(function (names) {
            if (!names.includes(_this.databaseName)) {
                console.log("create database ".concat(_this.databaseName));
                return self.influx.createDatabase(_this.databaseName);
            }
        })
            .catch(function (err) {
            console.error("Error creating Influx database!");
        });
    };
    DB.prototype.getTimeStamp = function (data) {
        var ts;
        if (data.source.type == 'opcua' && data.measure.sourceTimestamp) {
            if (data.measure.sourcePicoseconds) {
                ts = Date.parse(data.measure.sourceTimestamp).toString() + data.measure.sourcePicoseconds.toString();
                ts = ts.substr(0, 19);
            }
            else {
                ts = Date.parse(data.measure.sourceTimestamp).toString();
                ts = ts.substr(0, 19);
            }
        }
        if (data.timestamp) {
            ts = data.timestamp * 1000000;
        }
        return ts;
    };
    DB.prototype.insert = function (data) {
        //        console.log(data);
        if (typeof data !== "object") {
            console.error('payload is not an object');
            return false;
        }
        try {
            var ts = this.getTimeStamp(data);
            this.influx.writePoints([
                {
                    measurement: data.metric.name,
                    tags: data.metric.tags,
                    fields: { value: data.measure.value.value },
                    timestamp: ts
                }
            ]).catch(function (err) {
                console.error("Error saving data to InfluxDB! ".concat(err.stack));
            });
        }
        catch (error) {
            console.error(error);
        }
    };
    return DB;
}());
exports.DB = DB;
