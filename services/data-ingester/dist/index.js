"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mqtt_adapter_1 = require("./mqtt-adapter");
var influx_1 = require("./influx");
var mqttBroker = process.env.MQTT_BROKER || 'mqtt://localhost';
var applicationName = process.env.APPLICATION_NAME || 'default';
var serviceName = process.env.SERVICE_NAME || 'default';
var instanceName = process.env.INSTANCE_NAME || 'default';
var databaseHost = process.env.DATABASE_HOST || 'localhost';
console.log("starting app");
var mqtt = new mqtt_adapter_1.MqttAdapter(mqttBroker, applicationName);
var db = new influx_1.DB(databaseHost, applicationName);
mqtt.subscribe("".concat(applicationName, "/measure/write/influx"), function (topic, message) {
    var data = JSON.parse(message);
    db.insert(data);
});
