#!/bin/sh

HOME=$PWD

docker network create bredge

cd $HOME/.dev/typemon && npm run build

cd $HOME/services/opcua-adapter && npm run build
cd $HOME/services/opcua-manager && npm run build.dev
cd $HOME/services/data-ingester && npm run build
